const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

console.log('process.env.NODE_ENV =', process.env.NODE_ENV); // 打印环境变量
function resolve(dir) {
  return path.join(__dirname, dir);
}
const config = {
  entry: './src/index.js', // 打包入口地址
  output: {
    filename: 'bundle.js', // 输出文件名
    path: path.join(__dirname, 'dist'), // 输出文件目录
  },
  resolve: {
    // 配置别名
    alias: {
      '~': resolve('src'),
      '@': resolve('src'),
      'components': resolve('src/components'),
    },
    extensions: ['.ts', '...'],
    modules: [resolve('src'), 'node_modules'],
  },
  externals: {
    jquery: 'jQuery',
  },
  devtool: 'eval-cheap-module-source-map',
  devServer: {
    static: path.resolve(__dirname, 'public'), // 静态文件目录
    compress: true, // 是否启动压缩 gzip
    port: 8080, // 端口号
  },
  module: {
    noParse: /jquery|lodash/,
    rules: [
      {
        test: /\.js$/i,
        include: resolve('src'),
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
            },
          },
        ],
      },
      {
        test: /\.(s[ac]|c)ss$/i, // 匹配所有的 scss 文件
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader'],
      },
      // {
      //   test: /\.(jpe?g|png|gif)$/i, // 匹配图片文件
      //   use: [
      //     'file-loader', // 使用 file-loader
      //   ],
      // },
      {
        test: /\.png/,
        type: 'asset/resource',
      },
    ],
  },
  plugins: [
    // 配置插件
    new MiniCssExtractPlugin({
      // 添加插件
      filename: '[name].[hash:8].css',
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
    }),
    new CleanWebpackPlugin(),
  ],
};

module.exports = (env, argv) => {
  console.log('argv.mode=', argv.mode); // 打印 mode(模式) 值
  // 这里可以通过不同的模式修改 config 配置
  return config;
};
